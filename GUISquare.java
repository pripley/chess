import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.event.*;

public class GUISquare {
  private int m_X;
  private int m_Y;
  private Image m_Image;
  private boolean m_HasImage = false;
  GUISquare() {}
  
  public void setImage(Image i) {
    m_Image = i;
    m_HasImage = true;
  }
  
  public boolean hasImage() { return m_HasImage; }
  public void removeImage() { m_HasImage = false; }
  public Image getImage() { return m_Image; }
}