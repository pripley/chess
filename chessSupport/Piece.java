package chessSupport;

import java.io.*;

public interface Piece {
  public boolean move(ChessSquare dest, ChessSquare origin) throws MovementException;
  public boolean capture(ChessSquare dest, ChessSquare origin) throws MovementException;
  public int getColour();
  public int getPieceType();
  public boolean hasMoved();
}