package chessSupport;

import java.io.*;

public class Rook implements Piece{
  
  private int m_Colour;
  private boolean m_HasMoved = false;
  public Rook( int colour) {
     m_Colour= colour;
  }

  public int getPieceType() {
    return ChessConstants.ROOK;
  }
  
  public boolean move(ChessSquare dest, ChessSquare origin) throws MovementException{
    int x = Math.abs(dest.getX() - origin.getX());
    int y = Math.abs(dest.getY() - origin.getY());
    if ((x==y) || ((x>0) && (y>0))) {
      throw new MovementException();
    }
    ChessSquare checkSq;
    checkSq = origin;
    if (x!=0) {
      for (int j=1; j<x; j++) {
        
        if (checkSq.getX() > dest.getX()) {
          checkSq = checkSq.getWestSq();
        }
        if (checkSq.getX() < dest.getX()) {
          checkSq = checkSq.getEastSq();
        } 
      
        if (checkSq.isOccupied()) { throw new MovementException();}  
      }
    }
    else {
      for (int j=1; j<y; j++) {
        
        if (checkSq.getY() > dest.getY()) {
          checkSq = checkSq.getSouthSq();
        }
        if (checkSq.getY() < dest.getY()) {
          checkSq = checkSq.getNorthSq();
        } 
      
        if (checkSq.isOccupied()) { throw new MovementException();}  
      }
    }
    m_HasMoved = true;
    dest.setPiece(this);
    return true;
  }

  public boolean hasMoved() {
    return m_HasMoved;
  }


  public boolean capture(ChessSquare dest, ChessSquare origin) throws MovementException {
    if (dest.getPiece().getColour() == origin.getPiece().getColour()) { 
      throw new MovementException("You cannot capture your own piece");
      
    }
    return move(dest, origin);
  }

  public int getColour() {
    return m_Colour;
  }

}