package chessSupport;

public class PieceNotYoursException extends Exception {
  public PieceNotYoursException(String s) {
    new Exception(s);
  }
  public PieceNotYoursException() {
    new Exception();
  }
}