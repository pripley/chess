package chessSupport;

import java.io.*;

public class Pawn implements Piece{
  
  private int m_Colour;
  private boolean m_HasMoved = false;
  private boolean m_MoveUp;
  private MessageManager m_MessageManager;

  public Pawn( int colour, boolean moveUp, MessageManager m) {
     m_Colour= colour;
     m_MoveUp = moveUp;
     m_MessageManager = m;
  }

  public boolean hasMoved() { return m_HasMoved; }
  public int getPieceType() {
    return ChessConstants.PAWN;
  }
  
  public boolean move(ChessSquare dest, ChessSquare origin) throws MovementException{
    int x = Math.abs(dest.getX() - origin.getX());
    int y = Math.abs(dest.getY() - origin.getY());
    if (((x>0) || (y>2)) || ((y==2) && (m_HasMoved))) {
      throw new MovementException();
    }
    if (((dest.getY() > origin.getY()) && (!m_MoveUp)) || ((dest.getY() < origin.getY()) && (m_MoveUp))) {
      throw new MovementException();
    }
    ChessSquare checkSq;
    checkSq = origin;
        
    if (checkSq.getY() > dest.getY()) {
      checkSq = checkSq.getSouthSq();
    }
    if (checkSq.getY() < dest.getY()) {
      checkSq = checkSq.getNorthSq();
    }      
        
    if (checkSq.isOccupied()) { throw new MovementException();}  
    if (y==2) {
      if (checkSq.getY() > dest.getY()) {
        checkSq = checkSq.getSouthSq();
      }
      if (checkSq.getY() < dest.getY()) {
        checkSq = checkSq.getNorthSq();
      }
      if (checkSq.isOccupied()) { throw new MovementException();}
      //check for pawn passing
      if (dest.getX() > 1) {
        if (checkSq.getWestSq().isOccupied()) {
           Piece p = checkSq.getWestSq().getPiece();
           if ((p.getColour() != origin.getPiece().getColour()) && (p.getPieceType() == ChessConstants.PAWN)) {
             System.out.println("No pawn passing allowed");
             throw new MovementException();
           }
        }
      } 
      if (dest.getX() < 8) {
        if (checkSq.getEastSq().isOccupied()) {
           Piece p = checkSq.getEastSq().getPiece();
           if ((p.getColour() != origin.getPiece().getColour()) && (p.getPieceType() == ChessConstants.PAWN)) {
             System.out.println("No pawn passing allowed");
             throw new MovementException();
           }
        }
      } 

    }
    m_HasMoved = true;
        //if pawn reaches the final row it becomes a queen
    if ((dest.getY() == 1) || (dest.getY() == 8)) {
      ChessMessage c = new ChessMessage(ChessConstants.QUEEN, dest.getX(), dest.getY());
      dest.setPiece(new Queen(getColour()));
      m_MessageManager.addMessage(c);     
    }
    else {
      dest.setPiece(this);
    }
    return true;
  }


  public boolean capture(ChessSquare dest, ChessSquare origin) throws MovementException {
    if (dest.getPiece().getColour() == origin.getPiece().getColour()) { 
      throw new MovementException("You cannot capture your own piece"); 
    }
    if (((dest.getY() > origin.getY()) && (!m_MoveUp)) || ((dest.getY() < origin.getY()) && (m_MoveUp))) {
      throw new MovementException();
    }
    int x = Math.abs(dest.getX() - origin.getX());
    int y = Math.abs(dest.getY() - origin.getY());
    if ((x!=1) || (y!=1)) {
      throw new MovementException();
    }
    m_HasMoved = true;
    //if pawn reaches the final row it becomes a queen
    if ((dest.getY() == 1) || (dest.getY() == 8)) {
      ChessMessage c = new ChessMessage(ChessConstants.QUEEN, dest.getX(), dest.getY());
      dest.setPiece(new Queen(getColour()));
      m_MessageManager.addMessage(c);     
    }
    else {
      dest.setPiece(this);
    }
    return true;
  }

  public int getColour() {
    return m_Colour;
  }

}