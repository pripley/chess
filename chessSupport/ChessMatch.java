package chessSupport;

import java.io.*;

public class ChessMatch {
  private ChessBoard m_ChessBoard;
  //private int m_Turn;
  
  public ChessMatch(MessageManager m) {
    m_ChessBoard = new ChessBoard(m);
    
  }
  
  public static void main(String arg[]) {
    ChessMatch c = new ChessMatch(new MessageManager());
  }
  
  public boolean movePiece(int originx, int originy, int destx, int desty, int colour) {
    try {
      if (m_ChessBoard.movePiece(originx, originy, destx, desty, colour)) {
        //if (m_Turn == ChessConstants.WHITE) { m_Turn = ChessConstants.BLACK; } else { m_Turn = ChessConstants.WHITE; }
        return true;
      }
      else {
        return false;
      }
    } catch(PieceNotYoursException e) {
       System.out.println("The piece you are trying to move is not yours.");
       //e.printStackTrace();
       return false;
    }
    catch(MovementException e) {
       System.out.println("You made a movement error.");
       //e.printStackTrace();
       return false;
    }
    catch(SquareDoesNotExistException e) {
       System.out.println("One of the squares you selected does not exist.");
       //e.printStackTrace();
       return false;
    }
    catch(NoPieceThereException e) {
       System.out.println("There is no piece on the square you selected.");
       //e.printStackTrace();
       return false;
    }
  }
}