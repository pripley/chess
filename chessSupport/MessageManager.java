package chessSupport;

import java.util.*;

public class MessageManager {
  private ArrayList m_Messages;  
  public MessageManager(){ m_Messages = new ArrayList();}

  public void addMessage(ChessMessage c) {
    m_Messages.add(c);
  }
  
  public boolean hasMessages() {
    if (m_Messages.isEmpty()) {
      return false;
    }
    else {
      return true;
    } 
  }

  public ChessMessage getNextMessage() {
   ChessMessage c = (ChessMessage)m_Messages.get(0);
   m_Messages.remove(0);
   return c; 
  }
 
}