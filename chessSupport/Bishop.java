package chessSupport;

import java.io.*;
public class Bishop implements Piece{


  private int m_Colour;
  
  public Bishop(int colour) {
     m_Colour= colour;
  }

  public boolean hasMoved() {
    return false;
  }

  public int getPieceType() {
    return ChessConstants.BISHOP;
  }
  
  public boolean move(ChessSquare dest, ChessSquare origin) throws MovementException{
    int x = Math.abs(dest.getX() - origin.getX());
    int y = Math.abs(dest.getY() - origin.getY());
    if (x!=y) {
      throw new MovementException();
    }
    ChessSquare checkSq;
    checkSq = origin;
    for (int j=1; j<x; j++) {
//      for (int i=1; i<y; i++) {
        
        if ((checkSq.getX() > dest.getX()) && (checkSq.getY() > dest.getY())) {
          checkSq = checkSq.getSouthSq();
          checkSq = checkSq.getWestSq();
        }
        if ((checkSq.getX() > dest.getX()) && (checkSq.getY() < dest.getY())) {
          checkSq = checkSq.getNorthSq();
          checkSq = checkSq.getWestSq();
        } 
        if ((checkSq.getX() < dest.getX()) && (checkSq.getY() > dest.getY())) {
          checkSq = checkSq.getSouthSq();
          checkSq = checkSq.getEastSq();
        }
        if ((checkSq.getX() < dest.getX()) && (checkSq.getY() < dest.getY())) {
          checkSq = checkSq.getNorthSq();
          checkSq = checkSq.getEastSq();
        } 
        if (checkSq.isOccupied()) { throw new MovementException();}  
      }
//  }
    dest.setPiece(this);
    return true;
  }

  public boolean capture(ChessSquare dest, ChessSquare origin) throws MovementException {
    if (dest.getPiece().getColour() == origin.getPiece().getColour()) { 
      System.out.println("Capture error");
      throw new MovementException("You cannot capture your own piece");
      
    }
    return move(dest, origin);
  }

  public int getColour() {
    return m_Colour;
  }

}