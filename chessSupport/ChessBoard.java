package chessSupport;

import java.io.*;
import java.util.*;

public class ChessBoard {
  public static final int BOARDSIZE = 8;

  private Collection ChessSquareCollection;
  
  public ChessBoard(MessageManager m) {
    ChessSquareCollection = new ArrayList();
    buildBoard();
    addPieces(m);
  }
  
  private void buildBoard() {
    
    
    for (int i=1; i<=BOARDSIZE; i++) {
      for (int j=1; j<=BOARDSIZE; j++) {
        ChessSquare c = new ChessSquare(i, j);
        
        ChessSquareCollection.add(c);
      }  
    }
    try {
    for (int i=1; i<=BOARDSIZE; i++) {
      for (int j=1; j<=BOARDSIZE; j++) {
        if (j > 1) {
          getSquare(i,j).setSouthSq(getSquare(i, j-1));
        }
        if (i > 1) {
          getSquare(i,j).setWestSq(getSquare(i-1, j));
        }
        if (j < BOARDSIZE) {
          getSquare(i,j).setNorthSq(getSquare(i, j+1));
        } 
        if (i < BOARDSIZE) {
          getSquare(i,j).setEastSq(getSquare(i+1, j));
        } 
      }
    }
   } catch(SquareDoesNotExistException e) {
     System.out.println("A problem occurred creating the board");
   }
 }
  
  public boolean movePiece(int originx, int originy, int destx, int desty, int colour) throws NoPieceThereException, SquareDoesNotExistException, MovementException, PieceNotYoursException {
   
      ChessSquare dest = getSquare(destx, desty);
      ChessSquare origin = getSquare(originx, originy);
      return dest.movePieceHere( origin, colour);
    
  }

  private ChessSquare getSquare(int x, int y) throws SquareDoesNotExistException {
    Iterator i = ChessSquareCollection.iterator();
    while (i.hasNext()) {
      ChessSquare c = (ChessSquare)i.next();
      if ((c.getX() == x) && (c.getY() == y)) {
        return c;
      }
    }
    throw new SquareDoesNotExistException();
  }

  public ChessSquare getBottomLeftSq() throws SquareDoesNotExistException {
     return getSquare(1,1);
  }

  public ChessSquare getTopLeftSq() throws SquareDoesNotExistException { 
      return getSquare(1,8);
  }

  private void addPieces(MessageManager m) {
   try {
      //White pieces
      Rook r1 = new Rook(ChessConstants.WHITE);
      getSquare(1,1).setPiece(r1);
      Bishop b1 = new Bishop(ChessConstants.WHITE);
      getSquare(3,1).setPiece(b1);
      Knight k1 = new Knight(ChessConstants.WHITE);
      getSquare(2,1).setPiece(k1);
      Rook r2 = new Rook(ChessConstants.WHITE);
      getSquare(8,1).setPiece(r1);
      Bishop b2 = new Bishop(ChessConstants.WHITE);
      getSquare(6,1).setPiece(b2);
      Knight k2 = new Knight(ChessConstants.WHITE);
      getSquare(7,1).setPiece(k2);
      Queen q1 = new Queen(ChessConstants.WHITE);
      getSquare(4,1).setPiece(q1);
      King king1 = new King(ChessConstants.WHITE, m);
      getSquare(5,1).setPiece(king1);
      for (int i=1; i<=8; i++) {
         Pawn p = new Pawn(ChessConstants.WHITE, true, m);
         getSquare(i,2).setPiece(p);
      }
      //Black pieces
      Rook r3 = new Rook(ChessConstants.BLACK);
      getSquare(1,8).setPiece(r3);
      Bishop b3 = new Bishop(ChessConstants.BLACK);
      getSquare(3,8).setPiece(b3);
      Knight k3 = new Knight(ChessConstants.BLACK);
      getSquare(2,8).setPiece(k3);
      Rook r4 = new Rook(ChessConstants.BLACK);
      getSquare(8,8).setPiece(r4);
      Bishop b4 = new Bishop(ChessConstants.BLACK);
      getSquare(6,8).setPiece(b4);
      Knight k4 = new Knight(ChessConstants.BLACK);
      getSquare(7,8).setPiece(k4);
      Queen q2 = new Queen(ChessConstants.BLACK);
      getSquare(4,8).setPiece(q2);
      King king2 = new King(ChessConstants.BLACK, m);
      getSquare(5,8).setPiece(king2);
      for (int i=1; i<=8; i++) {
         Pawn p = new Pawn(ChessConstants.BLACK, false, m);
         getSquare(i,7).setPiece(p);
      }

    } catch(SquareDoesNotExistException e) {
      System.out.println("A problem occurred creating the board");
    }
  }
}