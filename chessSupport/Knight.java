package chessSupport;

import java.io.*;

public class Knight implements Piece{

  private int m_Colour;
  
  public Knight(int colour) {
     m_Colour= colour;
  }

  public int getPieceType() {
    return ChessConstants.KNIGHT;
  }

  public boolean hasMoved() {
    return false;
  }

  public boolean move(ChessSquare dest, ChessSquare origin) throws MovementException{
    
    int x = Math.abs(dest.getX() - origin.getX());
    int y = Math.abs(dest.getY() - origin.getY());
    if (((x==1) && (y==2))  || ((x==2) && (y==1))) {
      dest.setPiece(this);
      return true;
    }
    else {
      throw new MovementException("You cannot move a knight like that.");
    }
  }

  public boolean capture(ChessSquare dest, ChessSquare origin) throws MovementException {
    if (dest.getPiece().getColour() == origin.getPiece().getColour()) { 
      throw new MovementException("You cannot capture your own piece");
      
    }
    return move(dest, origin);
  }

  public int getColour() {
    return m_Colour;
  }
}