package chessSupport;

public class SquareDoesNotExistException extends Exception {
  public SquareDoesNotExistException(String s) {
    new Exception(s);
  }
  public SquareDoesNotExistException() {
    new Exception();
  }
}