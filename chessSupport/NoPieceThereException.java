package chessSupport;

public class NoPieceThereException extends Exception {
  public NoPieceThereException(String s) {
    new Exception(s);
  }
  public NoPieceThereException() {
    new Exception();
  }
}