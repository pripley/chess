package chessSupport;

import java.io.*;
public class King implements Piece{
  private MessageManager m_MessageManager;
  private int m_Colour;
  private boolean m_HasMoved = false;

  public King( int colour, MessageManager m) {
     m_Colour= colour;
     m_MessageManager = m;
  }
  
  public int getPieceType() {
    return ChessConstants.KING;
  }
  
  public boolean hasMoved() {
    return m_HasMoved;
  }

  public boolean move(ChessSquare dest, ChessSquare origin) throws MovementException{
    int x = Math.abs(dest.getX() - origin.getX());
    int y = Math.abs(dest.getY() - origin.getY());
    ChessSquare checkSq;
    checkSq = origin;

    //castling
    if ((y==0) && ((x>1) && (!m_HasMoved))) {

       //castling on the short side
       if (dest.getX() > origin.getX()) {
        for (int i=1; i<x; i++) {
            checkSq = checkSq.getEastSq();
            if (checkSq.isOccupied()) { throw new MovementException();}
        }
        checkSq = checkSq.getEastSq();

        if (checkSq.isOccupied()) {

          Piece p = checkSq.getPiece();
          if ((p.getPieceType() == ChessConstants.ROOK) && (!p.hasMoved()) && (p.getColour() == this.getColour())) {
            
            ChessMessage c = new ChessMessage(ChessConstants.ROOK, dest.getWestSq().getWestSq().getX(), dest.getY());
            m_MessageManager.addMessage(c);
            c = new ChessMessage(ChessConstants.KING, dest.getWestSq().getX(), dest.getY());
            m_MessageManager.addMessage(c);
            c = new ChessMessage(-1, dest.getX(), dest.getY());
            m_MessageManager.addMessage(c);
            checkSq.getWestSq().getWestSq().setPiece(new Rook(getColour()));
            checkSq.removePiece();
            checkSq.getWestSq().setPiece(this);
            checkSq.getWestSq().getWestSq().getWestSq().removePiece();

            m_HasMoved = true;            
            return true; 
          }
        }
        throw new MovementException(); 
      }
      
      else {
        //castle on the long side
        for (int i=1; i<x; i++) {
            checkSq = checkSq.getWestSq();
            if (checkSq.isOccupied()) { throw new MovementException();}
        }
        checkSq = checkSq.getWestSq();

        if (checkSq.isOccupied()) {

          Piece p = checkSq.getPiece();
          if ((p.getPieceType() == ChessConstants.ROOK) && (!p.hasMoved()) && (p.getColour() == this.getColour())) {
            
            ChessMessage c = new ChessMessage(ChessConstants.ROOK, origin.getWestSq().getX(), dest.getY());
            m_MessageManager.addMessage(c);
            c = new ChessMessage(ChessConstants.KING, dest.getEastSq().getEastSq().getX(), dest.getY());
            m_MessageManager.addMessage(c);
            c = new ChessMessage(-1, dest.getX(), dest.getY());
            m_MessageManager.addMessage(c);
            origin.getWestSq().setPiece(new Rook(getColour()));
            checkSq.removePiece();
            checkSq.getEastSq().getEastSq().setPiece(this);

            m_HasMoved = true;            
            return true; 
          }
        }
        throw new MovementException(); 
         
      }
    }
    //normal movement
    if ((x>1) || (y>1) || ((x==0) && (y==0))) {
      throw new MovementException();
    }
    checkSq = origin;
    
        
        if ((checkSq.getX() > dest.getX()) && (checkSq.getY() > dest.getY())) {
          checkSq = checkSq.getSouthSq();
          checkSq = checkSq.getWestSq();
        }
        if ((checkSq.getX() > dest.getX()) && (checkSq.getY() < dest.getY())) {
          checkSq = checkSq.getNorthSq();
          checkSq = checkSq.getWestSq();
        } 
        if ((checkSq.getX() < dest.getX()) && (checkSq.getY() > dest.getY())) {
          checkSq = checkSq.getSouthSq();
          checkSq = checkSq.getEastSq();
        }
        if ((checkSq.getX() < dest.getX()) && (checkSq.getY() < dest.getY())) {
          checkSq = checkSq.getNorthSq();
          checkSq = checkSq.getEastSq();
        } 
        if (y==0) {
          if (checkSq.getX() > dest.getX()) {
            checkSq = checkSq.getWestSq();
          }
          if (checkSq.getX() < dest.getX()) {
            checkSq = checkSq.getEastSq();
          }
        } 
        if (x==0) {
          if (checkSq.getY() > dest.getY()) {
            checkSq = checkSq.getSouthSq();
          }
          if (checkSq.getY() < dest.getY()) {
            checkSq = checkSq.getNorthSq();
          }
        } 
         
    m_HasMoved = true;
    dest.setPiece(this);
    return true;
  }


  public boolean capture(ChessSquare dest, ChessSquare origin) throws MovementException {
    if (dest.getPiece().getColour() == origin.getPiece().getColour()) { 
      if ((Math.abs(dest.getX() - origin.getX()) == 1) && (dest.getPiece().getPieceType() != ChessConstants.ROOK)) {
        throw new MovementException("You cannot capture your own piece");
      }
    }
    return move(dest, origin);
  }

  public int getColour() {
    return m_Colour;
  }

}