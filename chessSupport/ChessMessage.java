package chessSupport;

public class ChessMessage {
  private int m_PieceType;
  private int m_X;
  private int m_Y;

  public ChessMessage(int pieceType, int x, int y) {
    m_PieceType = pieceType;
    m_X = x;
    m_Y = y;
  }
  
  public int getX() {
   return m_X; 
  }

  public int getY() {
   return m_Y; 
  }
  public int getPieceType() {
    return m_PieceType;
  }
}