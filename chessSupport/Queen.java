package chessSupport;

import java.io.*;

public class Queen implements Piece{
  
  private int m_Colour;
  
  public Queen( int colour) {
     m_Colour= colour;
  }

  public boolean hasMoved() {
    return false;
  }

  public int getPieceType() {
    return ChessConstants.QUEEN;
  }
  
  public boolean move(ChessSquare dest, ChessSquare origin) throws MovementException{
    int x = Math.abs(dest.getX() - origin.getX());
    int y = Math.abs(dest.getY() - origin.getY());
    if ((x!=y) && ((x==y) || ((x>0) && (y>0)))) {
      throw new MovementException();
    }
    ChessSquare checkSq;
    checkSq = origin;
    
    if (y==0) {
      for (int j=1; j<x; j++) {    
        //System.out.println("checkSq: x:"+checkSq.getX()+" y:"+checkSq.getY()+" dest: x:"+dest.getX()+" y:"+dest.getY());
        if (checkSq.getX() > dest.getX()) {
          checkSq = checkSq.getWestSq();
        }
        if (checkSq.getX() < dest.getX()) {
          checkSq = checkSq.getEastSq();
        }
        if (checkSq.isOccupied()) { throw new MovementException();}  
      } 
    }
    if (x==0) {
     for (int k=1; k<y; k++) {
       if (checkSq.getY() > dest.getY()) {
         checkSq = checkSq.getSouthSq();
       }
       if (checkSq.getY() < dest.getY()) {
         checkSq = checkSq.getNorthSq();
       }
       if (checkSq.isOccupied()) { throw new MovementException();}  
     }
   } 
      for (int i=1; i<y; i++) {
        
        if ((checkSq.getX() > dest.getX()) && (checkSq.getY() > dest.getY())) {
          checkSq = checkSq.getSouthSq();
          checkSq = checkSq.getWestSq();
        }
        if ((checkSq.getX() > dest.getX()) && (checkSq.getY() < dest.getY())) {
          checkSq = checkSq.getNorthSq();
          checkSq = checkSq.getWestSq();
        } 
        if ((checkSq.getX() < dest.getX()) && (checkSq.getY() > dest.getY())) {
          checkSq = checkSq.getSouthSq();
          checkSq = checkSq.getEastSq();
        }
        if ((checkSq.getX() < dest.getX()) && (checkSq.getY() < dest.getY())) {
          checkSq = checkSq.getNorthSq();
          checkSq = checkSq.getEastSq();
        }
         
        if (checkSq.isOccupied()) { throw new MovementException();}  
      }
   dest.setPiece(this);
   return true;
  }


  public boolean capture(ChessSquare dest, ChessSquare origin) throws MovementException {
    if (dest.getPiece().getColour() == origin.getPiece().getColour()) { 
      throw new MovementException("You cannot capture your own piece");
      
    }
    return move(dest, origin);
  }

  public int getColour() {
    return m_Colour;
  }

}