import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.event.*;
import java.util.*;
import chessSupport.*;

public class GUIChessBoard extends JPanel {
  boolean m_Clicked = false;
  int m_Colour;
  private ArrayList m_PieceCollection;
  private ChessMatch m_Match;
  private MediaTracker m_Tracker;
  private final JLabel m_Turn;
  public GUIChessBoard() {
    
    m_Tracker = new MediaTracker(this); 
    m_PieceCollection = new ArrayList();
    m_PieceCollection.add(new Integer(0));
    for (int j=1; j<=8; j++) { 
      ArrayList a = new ArrayList();
      a.add(new Integer(0));
      for (int k=1; k<=8; k++) { 
        
        GUISquare s = new GUISquare();
        a.add(k,s);
          
      }
      m_PieceCollection.add(j, a);
    }
    JLabel turnLabel = new JLabel("Turn: ");
    m_Turn = new JLabel("White");
    
    add(turnLabel);
    add(m_Turn);
    addMouseListener(new MouseAdapter() {
       private int originX;
       private int originY;
       private int colourTurn = ChessConstants.WHITE;
       MessageManager m_MessageManager = new MessageManager();
       ChessMatch m_Match = new ChessMatch(m_MessageManager);
        

       public void mousePressed(MouseEvent e) {

        originX = e.getX()/51;
        originY = e.getY()/51;
        if ((originX > 8) || (originY > 8)) { return; }
        if (((GUISquare)((ArrayList)m_PieceCollection.get(originX)).get(originY)).hasImage()) {
          //System.out.println("An image is here");
        }
      }
      
      public void mouseReleased(MouseEvent e) {
        System.out.println("origin:"+originX+", "+originY);
        System.out.println("Released on X:"+ (e.getX()/51) + " Y:"+ e.getY()/51);
        int destx = e.getX()/51;
        int desty = e.getY()/51;
        if (m_Match.movePiece(originX, 9 - originY, destx, 9 - desty, colourTurn)) {
          System.out.println("Good Move");
          GUISquare g = (GUISquare)(((ArrayList)m_PieceCollection.get(originX)).get(originY));
         ((GUISquare)((ArrayList)m_PieceCollection.get(destx)).get(desty)).setImage(g.getImage());
         g.removeImage();
         //check for messages eg castling, queening
         ChessMessage c;
         while(m_MessageManager.hasMessages()) {
           c = m_MessageManager.getNextMessage();
           //((GUISquare)((ArrayList)m_PieceCollection.get(c.getX())).get(9 - c.getY())).setImage(g.getImage());
           makeImage(c.getPieceType(), c.getX(), 9 - c.getY(), colourTurn);
         }
         //repaint(); 
         updateUI();
         if (colourTurn == ChessConstants.BLACK) {
           m_Turn.setText("White");
           colourTurn = ChessConstants.WHITE;
         } else {
           m_Turn.setText("Black");
           colourTurn = ChessConstants.BLACK;
         }
         
        }
      }
    });    
  }

  public Dimension getPreferredSize() {
    return new Dimension(500, 500);
  }

  public void init() { 
   
         
//black rooks
    Image rb1 = (Toolkit.getDefaultToolkit()).getImage("images\\rookImg.gif");
    track(rb1);     
    ((GUISquare)((ArrayList)m_PieceCollection.get(1)).get(1)).setImage(rb1);
    ((GUISquare)((ArrayList)m_PieceCollection.get(8)).get(1)).setImage(rb1);
//black knights
    Image kb1 = (Toolkit.getDefaultToolkit()).getImage("images\\knightImg.gif");
    track(kb1);     
    ((GUISquare)((ArrayList)m_PieceCollection.get(2)).get(1)).setImage(kb1);
    ((GUISquare)((ArrayList)m_PieceCollection.get(7)).get(1)).setImage(kb1);
//black bishops
    Image bb1 = (Toolkit.getDefaultToolkit()).getImage("images\\bishopImg.gif");
    track(bb1);     
    ((GUISquare)((ArrayList)m_PieceCollection.get(3)).get(1)).setImage(bb1);
    ((GUISquare)((ArrayList)m_PieceCollection.get(6)).get(1)).setImage(bb1);
//black pawns
    for (int k=1; k<=8; k++) {
      makeImage(ChessConstants.PAWN, k, 2,ChessConstants.BLACK);
    }  
//black queen
    makeImage(ChessConstants.QUEEN, 4, 1,ChessConstants.BLACK); 
//black king
    makeImage(ChessConstants.KING, 5, 1,ChessConstants.BLACK);
//white rooks
    makeImage(ChessConstants.ROOK, 1, 8,ChessConstants.WHITE);
    makeImage(ChessConstants.ROOK, 8, 8,ChessConstants.WHITE);
//white knights
    makeImage(ChessConstants.KNIGHT, 2, 8,ChessConstants.WHITE);
    makeImage(ChessConstants.KNIGHT, 7, 8,ChessConstants.WHITE);
//white bishops
    makeImage(ChessConstants.BISHOP, 3, 8,ChessConstants.WHITE);
    makeImage(ChessConstants.BISHOP, 6, 8,ChessConstants.WHITE);
//white pawns
    for (int k=1; k<=8; k++) {    
      makeImage(ChessConstants.PAWN, k, 7,ChessConstants.WHITE);
    }  
//white queen
    makeImage(ChessConstants.QUEEN, 4, 8,ChessConstants.WHITE); 
//white king
    makeImage(ChessConstants.KING, 5, 8,ChessConstants.WHITE);
  }

  public void makeImage(int pieceType, int x, int y, int colour) {
      if (pieceType == -1) { 
        ((GUISquare)((ArrayList)m_PieceCollection.get(x)).get(y)).removeImage();  
        return;   
      }
      Image i = (Toolkit.getDefaultToolkit()).getImage("images\\pawnImg.gif");
      switch(pieceType) {
      case ChessConstants.PAWN: if (colour == ChessConstants.BLACK) {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\pawnImg.gif");
                                } else {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\wpawnImg.gif");
                                }
                                break;
                              
      case ChessConstants.ROOK: if (colour == ChessConstants.BLACK) {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\rookImg.gif");
                                } else {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\wrookImg.gif");
                                }
                                break;
      case ChessConstants.KNIGHT: if (colour == ChessConstants.BLACK) {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\knightImg.gif");
                                } else {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\wknightImg.gif");
                                }
                                break;
      case ChessConstants.BISHOP: if (colour == ChessConstants.BLACK) {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\bishopImg.gif");
                                } else {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\wbishopImg.gif");
                                }
                                break;
      case ChessConstants.QUEEN: if (colour == ChessConstants.BLACK) {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\queenImg.gif");
                                } else {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\wqueenImg.gif");
                                }
                                break;
      case ChessConstants.KING: if (colour == ChessConstants.BLACK) {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\kingImg.gif");
                                } else {
                                  i = (Toolkit.getDefaultToolkit()).getImage("images\\wkingImg.gif");
                                }
                                break;
                                  
    }
    track(i);
    ((GUISquare)((ArrayList)m_PieceCollection.get(x)).get(y)).setImage(i);  

    
  } 
 
  private void track(Image i) {
    m_Tracker.addImage(i, 0);
    try { m_Tracker.waitForID(0); }
    catch (InterruptedException exception) {}
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);  //paint background 

    for (int i=1; i<=8; i++) {
      for (int j=1; j<=8; j++) {
        if (((i+j) % 2) == 0) {
          //want black square
          g.setColor(Color.black);
        } else {
          g.setColor(Color.white);
        }
        
          g.fillRect(i*50,j*50, 50, 50);
                
      }
    }  
      for (int i=1; i<=8; i++) {
        for (int j=1; j<=8; j++) {
           GUISquare p = (GUISquare)((ArrayList)m_PieceCollection.get(i)).get(j);
           if (p.hasImage()) {
             g.drawImage(p.getImage(), i*50, j*50, 50, 50, null);
           }
        } 
      } 
  }

}