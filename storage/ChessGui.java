import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import javax.swing.event.*;

public class ChessGui {
  private JFrame m_F;
  private Dimension preferredSize = new Dimension(350, 550);  
  private Collection m_Squares;
  public ChessGui() {
    m_Squares = new ArrayList();
  }
  private void buildUI() {
  
    m_F = new JFrame("Chess");
    m_F.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }  
    });
    drawBoard();
    //new ChessMatch(m_F);
        m_F.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        System.out.println("Position:"+e.getX()+", "+e.getY());
        Iterator i = m_Squares.iterator();
        while (i.hasNext()) {
          GUISquare c = (GUISquare)i.next();
          c.unselect();
        }
      }
    });
    m_F.pack();
    m_F.setVisible(true);
  }
  
  public void drawBoard() {
    GUISquare b;
    Container c = m_F.getContentPane();
    c.setLayout(new GridLayout(0,8));
    for (int i=1; i<=8; i++) {
      for (int j=1; j<=8; j++) {
        if (((i+j) % 2) == 0) {
          //want black square
          b = new GUISquare(50*i, 50*j, ChessConstants.BLACK, i, j);
        }
        else {
          b = new GUISquare(50*i, 50*j, ChessConstants.WHITE, i, j);
        }
        c.add(b);
      }
    }
  }

  public Dimension getPreferredSize() {
    return preferredSize;
  }
  
  public void paintComponent(Graphics g) {
    
    
  }

  public static void main(String args[]) {
    System.out.println("Starting GUI");
    ChessGui g = new ChessGui();
    g.buildUI();
  }
  
  
}