import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import javax.swing.event.*;

public class IFrame extends JInternalFrame
//public class IFrame extends JFrame
{

private String filename;
  private Image image;
  ImagePanel panel;
  int width;
  int height;

    IFrame(String str)
    {
         super(str,
        true, // resizable
        true, // closable
        true, // maximizable
        true); // iconifiable

    addInternalFrameListener (new InternalFrameAdapter() {
         public void internalFrameOpened(InternalFrameEvent e) {
width=(int)panel.getSize().getWidth();
height=(int)panel.getSize().getHeight();
         }
    });
                   filename=str;
panel=new ImagePanel(filename);
   getContentPane().add(new JScrollPane(panel));

    pack();
    }
         public int getIFrameWidth()
              {


                   return width;
    }
    public int getIFrameHeight()
    {
         return height;

    }

}
class ImagePanel extends JLabel
{
    private Image image;
    int imageWidth;
    int imageHeight;

public ImagePanel(String str)
 {
    // acquire the image
    image = Toolkit.getDefaultToolkit().getImage
       (str);
     MediaTracker tracker = new MediaTracker(this);
    tracker.addImage(image, 0);
    try { tracker.waitForID(0); }
    catch (InterruptedException exception) {}
    imageWidth = image.getWidth(this);
    imageHeight = image.getHeight(this);
    setPreferredSize(new Dimension(imageWidth, imageHeight));
 }
  public void paintComponent(Graphics g)
  {
     super.paintComponent(g);
     Graphics2D g2 = (Graphics2D)g;
     // draw the image in the upper-left corner
     g2.drawImage(image, 0, 0, null);

     // tile the image across the panel


  }


}
