import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.event.*;

public class GUISquare extends JPanel {
  boolean m_Clicked = false;
  int m_Colour;
  int m_XStartPos;
  int m_YStartPos;
  int m_X;
  int m_Y;  

  public GUISquare(int cordx, int cordy, int c, int x, int y) {
    m_XStartPos = cordx;
    m_YStartPos = cordy;  
    m_X = x;
    m_Y = y;
    m_Colour = c;
    addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        System.out.println("Position:"+e.getX()+", "+e.getY()+" square:"+m_X+", "+m_Y);
        m_Clicked = true;
        repaint();
      }
    });
  }
  public void unselect() {
    if (m_Clicked) {
      m_Clicked = false;
      repaint();
    }
  }

  public void paintComponent(Graphics g) {
   super.paintComponent(g);  //paint background 
        if (m_Colour == ChessConstants.BLACK) {g.setColor(Color.black); } else { g.setColor(Color.white);}
    
    //System.out.println("drawing rectangle:"+m_XStartPos+ ", "+m_YStartPos);
    //g.fillRect(m_XStartPos, m_YStartPos, 50, 50);
    if (m_Clicked) {
      g.fillRect(2,2, 48, 48);
      g.setColor(Color.blue);
      g.drawRect(0,0, 50, 50);
    }
    else {
      g.fillRect(0,0, 50, 50);
    }
    
  }

}