import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

public class ChessSquare extends JPanel {
  private int m_Colour;
  private int m_X;  
  private int m_Y;
  private int m_XStartPos;
  private int m_YStartPos;
  private ChessSquare m_NorthSq = null;
  private ChessSquare m_SouthSq = null;
  private ChessSquare m_WestSq = null;
  private ChessSquare m_EastSq = null;
  Dimension preferredSize = new Dimension(50,50);

  private int m_Boardsize;
  public ChessSquare(int squareColour, int x, int y, int boardsize, int xStartPos, int yStartPos) {
    m_Colour = squareColour;
    m_X = x;
    m_Y = y;
    m_Boardsize = boardsize;
    m_XStartPos = xStartPos;
    m_YStartPos = yStartPos;
    addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        System.out.println("Position:"+e.getX()+", "+e.getY()+" square:"+m_X+", "+m_Y);
      }
    });
  }
  
  public void setNorthSq(ChessSquare c) {
    m_NorthSq = c;
  }
  public void setSouthSq(ChessSquare c) {
    m_SouthSq = c;
  }
  public void setWestSq(ChessSquare c) {
    m_WestSq = c;
  }
  public void setEastSq(ChessSquare c) {
    m_EastSq = c;
  }

  public ChessSquare getNorthSq() {
    return m_NorthSq;
  }
  public ChessSquare getSouthSq() {
    return m_SouthSq;
  }
  public ChessSquare getWestSq() {
    return m_WestSq;
  }
  public ChessSquare getEastSq() {
    return m_EastSq;
  }

  public int getColour(){
    return m_Colour;
  }
  /**
  public int getX(){
    return m_X;
  }
  public int getY(){
    return m_Y;
  }
*/
  public boolean isOccupied() {
    return false;
  }
  
  public Dimension getPreferredSize() {
    return preferredSize;
  }

  public void paintComponent(Graphics g) {
    if (m_Colour == 0) {g.setColor(Color.black); } else { g.setColor(Color.white);}
    //g.drawRect(super.getX(), super.getY(), getWidth(), getHeight());
    g.drawRect(m_XStartPos, m_YStartPos, 50, 50);
    //g.paintComponent(g);
    
    //int sqSize = m_Boardsize / 8;
    //g.fillRect(m_X*50, 600 - m_Y*50, 50, 50);
    //g.fillRect(super.getX(), super.getY(), getWidth(), getHeight());
  }


}