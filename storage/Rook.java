import java.io.*;
public class Rook implements Piece{
  private ChessSquare m_CurrentSq;
  private Side m_Side;
  private int m_Colour;
  
  public Rook(ChessSquare initialSquare, Side s, int c) {
     m_CurrentSq = initialSquare;
     m_Side = s;
     m_Colour= c;
  }
  
  public boolean move(ChessSquare sq) {
    int x = m_CurrentSq.getX() - sq.getX();
    int y = m_CurrentSq.getY() - sq.getY();
    if (!((x==0) || (y==0))) {
      return false;
    }
    ChessSquare checkSq;
    checkSq = m_CurrentSq;
    if (y==0) {
      for (int i=1; i<=x; i++) {
        if (x>0) {
          checkSq = checkSq.getSouthSq();
        }
        else {
          checkSq = checkSq.getNorthSq();
        }

        if (checkSq.isOccupied()) {
          if (i==x) {
            m_CurrentSq = checkSq; //capture piece
            return true;
          }
          else {
            return false;
          }
        }
      }
      m_CurrentSq = checkSq;  // no pieces are on the square  
      return true;
    }
    else {
      for (int i=1; i<=y; i++) {
        if (y>0) {
          checkSq = checkSq.getWestSq();
        }
        else {
          checkSq = checkSq.getEastSq();
        }

        if (checkSq.isOccupied()) {
          if (i==y) {
            m_CurrentSq = checkSq; //capture piece
            return true;
          }
          else {
            return false;
          }
        }
      }
      m_CurrentSq = checkSq;  // no pieces are on the square  
      return true;
    }
  }
}