import java.io.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;

public class ChessBoard extends JPanel {
  public static final int BOARDSIZE = 8;
  public static final int WHITE = 0;
  public static final int BLACK = 1;
  Dimension preferredSize = new Dimension(900, 650);
  private Collection ChessSquareCollection;
  
  public Dimension getPreferredSize() {
    return preferredSize;
  }
 
  public ChessBoard() {
    ChessSquareCollection = new ArrayList();
    drawBoard();
  }
  
  private void drawBoard() {
    int squareColour = WHITE;
    //call some draw routine stuff
    for (int i=1; i<=BOARDSIZE; i++) {
      //JPanel jp = new JPanel();
      //jp.setLayout(new BoxLayout(jp,BoxLayout.Y_AXIS));
      for (int j=1; j<=BOARDSIZE; j++) {
        ChessSquare c = new ChessSquare(squareColour, i, j,BOARDSIZE,i*50,600 - j*50);
        //ChessSquare c = new ChessSquare(squareColour, i,j, 600);
        ChessSquareCollection.add(c);
        //jp.add(c);
        if (squareColour == WHITE) { squareColour = BLACK; } else { squareColour = WHITE; }
      }
        if (squareColour == WHITE) { squareColour = BLACK; } else { squareColour = WHITE; }
        //this.add(jp);
    }
    for (int i=1; i<=BOARDSIZE; i++) {
      for (int j=1; j<=BOARDSIZE; j++) {
        if (j > 1) {
          getSquare(i,j).setSouthSq(getSquare(i, j-1));
        }
        if (i > 1) {
          getSquare(i,j).setWestSq(getSquare(i-1, j));
        }
        if (j < BOARDSIZE) {
          getSquare(i,j).setNorthSq(getSquare(i, j+1));
        } 
        if (i < BOARDSIZE) {
          getSquare(i,j).setEastSq(getSquare(i+1, j));
        } 
      }
   }
   repaint();
 }
  
  public boolean movePiece(Piece p, ChessSquare sq) {
    return p.move(sq);
  }

  private ChessSquare getSquare(int x, int y) {
    Iterator i = ChessSquareCollection.iterator();
    while (i.hasNext()) {
      ChessSquare c = (ChessSquare)i.next();
      if ((c.getX() == x) && (c.getY() == y)) {
        return c;
      }
    }
    return null;
  }

  public ChessSquare getBottomLeftSq() {
     return getSquare(1,1);
  }
  public ChessSquare getTopLeftSq() {
     return getSquare(1,8);
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawRect(10, 10, 600, 600);

    Iterator i = ChessSquareCollection.iterator();
    while (i.hasNext()) {
      ChessSquare c = (ChessSquare)i.next();
      //c.paintComponent(g);
      
    }

  }
}