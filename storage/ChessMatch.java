import java.io.*;
import javax.swing.*;

public class ChessMatch {
  private ChessBoard m_ChessBoard;
  private int m_WhitesTurn;
  private Side m_North;
  private Side m_South;

  public ChessMatch(JFrame f) {
    ChessBoard m_ChessBoard = new ChessBoard();
    f.getContentPane().add(m_ChessBoard);
    m_North = new Side(m_ChessBoard, 0, 0, true);
    m_South = new Side (m_ChessBoard, 1, 1, false);
  }
  public static void main(String arg[]) {
    //new ChessMatch();
    System.out.println("Starting the match");
    //movePiece(b.getSquare(1,1).
  }
  public boolean movePiece(Piece p, ChessSquare sq) {
    return m_ChessBoard.movePiece(p,sq);
  }
}