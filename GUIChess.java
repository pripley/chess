import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import javax.swing.event.*;

public class GUIChess extends JFrame {
  private JFrame m_F;
    
  
  private GUIChessBoard m_Board;
    private void buildUI() {
  
    m_F = new JFrame("Chess");

    m_F.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }  
    });
    drawBoard();
    
    m_F.pack();
    m_F.setVisible(true);
  }
  
  public void drawBoard() {
    
    Container c = m_F.getContentPane();
    GUIChessBoard m_Board = new GUIChessBoard();
    m_Board.init();
    c.add(m_Board);
    
  }


  public static void main(String args[]) {
    System.out.println("Starting GUI");
    GUIChess g = new GUIChess();
    g.buildUI();
  }
  
  
}